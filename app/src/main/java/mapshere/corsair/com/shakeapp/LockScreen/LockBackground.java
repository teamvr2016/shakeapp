package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */
/**
 * Created by Tiash on 2/17/2016.
 */
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.widget.Toast;

import java.util.ArrayList;

import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;

/**
 * Created by Tiash on 2/12/2016.
 */
public class LockBackground extends Activity {
    private long mLastClickTime = 0;
    MediaPlayer tias;
    boolean sound = false;
    // private LockBackground lockBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if(bundle != null)
        {

            if (bundle.getBoolean("close")== true)
            {
                Toast.makeText(getApplicationContext(), "This should be closed", Toast.LENGTH_SHORT).show();
                if (tias != null)
                {
                    tias.stop();
                }
                finish();
            }
        }
        else
        {

            setContentView(R.layout.lockback);
            if (isMyServiceRunning(ChatHead.class) == false) {
                Toast.makeText(getApplicationContext(), "not running", Toast.LENGTH_SHORT).show();
                startService(new Intent(getApplicationContext(), ChatHead.class));
            } else {
                Toast.makeText(getApplicationContext(), "Already runing running", Toast.LENGTH_SHORT).show();
                //startService(new Intent(getApplicationContext(), ChatHead.class));
                // stopService(new Intent(getApplicationContext(),ChatHead.class));
            }
        }
        // startService(new Intent(getApplicationContext(), ChatHead.class));

    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_POWER))
        {
            AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
            String number1 = asp.getEmergencyNumber1();
            String number2 = asp.getEmergencyNumber2();
            String sms = asp.getEmergencySMS();
            sms = sms+" #ShakePatch";
           // Toast.makeText(getApplicationContext(),"Detected", Toast.LENGTH_LONG).show();
            SmsManager smsManager = SmsManager.getDefault();
            if (!number1.matches("null") || number1.equals("null")||number1.contains("null"))
            smsManager.sendTextMessage(number1, null, sms, null, null);
            if (!number2.matches("null") || number2.equals("null")||number2.contains("null"))
                smsManager.sendTextMessage(number2, null, sms, null, null);
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                String number1 = asp.getEmergencyNumber1();
                String number2 = asp.getEmergencyNumber2();
                String sms = asp.getEmergencySMS();
                sms = sms+" #ShakePatch";
               // Toast.makeText(getApplicationContext(),"Detected", Toast.LENGTH_LONG).show();
                SmsManager smsManager = SmsManager.getDefault();
                try {
                    if (!number1.matches("null") || number1.equals("null") || number1.contains("null"))
                        smsManager.sendTextMessage(number1, null, sms, null, null);
                    if (!number2.matches("null") || number2.equals("null") || number2.contains("null"))
                        smsManager.sendTextMessage(number2, null, sms, null, null);
                }
                catch (Exception e)
                {

                }

            }
            mLastClickTime = SystemClock.elapsedRealtime();

        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                //Toast.makeText(getApplicationContext(),"Detected too", Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(),playAlarm.class));
            }
            mLastClickTime = SystemClock.elapsedRealtime();

        }
        if (keyCode == KeyEvent.KEYCODE_BACK )
        {
            finish();
        }


        return true;
    }

    public void sendSMS()
    {

    }

}

