package mapshere.corsair.com.shakeapp.SlidingTab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mapshere.corsair.com.shakeapp.MoreFeatures;
import mapshere.corsair.com.shakeapp.R;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mapshere.corsair.com.shakeapp.MoreFeatures;
import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;

/**
 * Created by AlZihad on 8/27/2016.
 */
public class News extends Fragment
{
    VideoAdapter listAdapetr;
    ListView list;
    List topSongsList;
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.info_video_list, container, false);
        topSongsList = new ArrayList<Video>();

        /*Video temp1 = new Video("Animation-Earthquake Guide", "https://www.youtube.com/watch?v=30SFfPvcss0","www.youtube.com ");
        Video temp2 = new Video("Global Earthquake Animation", "https://www.youtube.com/watch?v=M3jHUGHOlhA","www.youtube.com ");
        Video temp3 = new Video("Disaster risk reduction (Earthquake) Animation" , "https://www.youtube.com/watch?v=F0pJ2Vhnxdc","www.youtube.com ");
        Video temp4 = new Video("Earthquake (short film)" , "https://www.youtube.com/watch?v=avvBpyh1kdE","www.youtube.com ");
        Video temp5 = new Video("Earthquake impact" , "https://www.youtube.com/watch?v=n9tdFipXGNY","www.youtube.com ");
        Video temp6 = new Video("Eartquake Animation" , "https://www.youtube.com/watch?v=Dsj792I4OQU","www.youtube.com ");
        Video temp7 = new Video("More about Earthquake" , "https://www.youtube.com/watch?v=4Xebwzb3dDE","www.youtube.com ");
        Video temp8 = new Video("Earthquake animation preview" , "https://www.youtube.com/watch?v=FXrPqLf_-WU","www.youtube.com ");
        Video temp9 = new Video("Earthquake preview" , "https://www.youtube.com/watch?v=VNUP5qh1PtY","www.youtube.com ");

        topSongsList.add(temp1);topSongsList.add(temp2);topSongsList.add(temp3);
        topSongsList.add(temp4);topSongsList.add(temp5);topSongsList.add(temp6);
        topSongsList.add(temp7);topSongsList.add(temp8);topSongsList.add(temp9);*/
        loadJSON();
        list = (ListView) v.findViewById(R.id.video_list);
        listAdapetr = new VideoAdapter(getActivity(), topSongsList);
        list.setAdapter(listAdapetr);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                startActivity(new Intent(getContext(),MoreFeatures.class));
            }
        });
        return v;
    }

    public void loadJSON()
    {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://ghorardim.ml/ShakeApp/contents/document.json",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // showToast(response.toString());
                       // Toast.makeText(getContext(),response.toString(),Toast.LENGTH_LONG).show();
                        Log.d("Response", response);
                        try {
                            JSONArray jArray2 = new JSONArray(response);
                            JSONArray jArray = jArray2.getJSONArray(0);
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);
                                String time = job.getString("sub");
                                String place =job.getString("title") ;
                                String link = job.getString("link");
                                Video v = new Video(place,link,time);
                                topSongsList.add(v);
                                listAdapetr.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Data loading error. Please check your connection...",Toast.LENGTH_LONG).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {

        };
        queue.add(postRequest);

    }
    public class VideoAdapter extends BaseAdapter
    {
        Activity activity;
        List<Video> topSong;

        public VideoAdapter(Activity activity, List<Video> topSong)
        {
            super();
            this.activity = activity;
            this.topSong = topSong;
        }

        @Override
        public int getCount() {
            return topSong.size();
        }

        @Override
        public Video getItem(int i) {
            return topSong.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup)
        {
            LayoutInflater inflater=activity.getLayoutInflater();
            view=inflater.inflate(R.layout.video_info_card,null);
            ImageView topSongsCover = (ImageView)view.findViewById(R.id.image);
            TextView title=(TextView) view.findViewById(R.id.title);
            TextView singer=(TextView) view.findViewById(R.id.description);
            Video topSongsForMainPage = topSong.get(i);
            title.setText(topSongsForMainPage.getTitle());
            singer.setText(topSongsForMainPage.getSubtitle());
            String url = topSongsForMainPage.getLink();

            topSongsCover.setImageResource(R.mipmap.watch);
            return view;
        }
    }






}
