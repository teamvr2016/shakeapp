package mapshere.corsair.com.shakeapp.Games;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import mapshere.corsair.com.shakeapp.R;

/**
 * Created by AlZihad on 9/7/2016.
 */
public class GameMenu extends Activity
{
    CardView game1, game2, game3;
    TextView gameName,gameName2,gameName3;
    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        setContentView(R.layout.game_menu);
        // set font
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/capss.otf");
        gameName = (TextView) findViewById(R.id.game_name);
        gameName.setTypeface(type);

        gameName2 = (TextView) findViewById(R.id.game_name2);
        gameName2.setTypeface(type);

        gameName3 = (TextView) findViewById(R.id.game_name3);
        gameName3.setTypeface(type);

        // in it cards
        game1 = (CardView) findViewById(R.id.game1);
        game2 = (CardView) findViewById(R.id.game2);
        game3 = (CardView) findViewById(R.id.game3);

        game1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.Shake)
                        .duration(500)
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                startActivity(new Intent(getApplicationContext(),Game1.class));
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(game1);
            }
        });
    }
}