package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.takwolf.android.lock9.Lock9View;

import mapshere.corsair.com.shakeapp.R;

/**
 * Created by Tiash on 2/15/2016.
 */
public class LockScreenSetup extends Activity {

    public Lock9View lock9View;
    int retry=0;
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locksetup);
        lock9View = (Lock9View) findViewById(R.id.lock_9_view);

        text = (TextView) findViewById(R.id.text);
        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password)
            {
                Intent tiash = new Intent(getApplicationContext(), LockConfirm.class);
                tiash.putExtra("pattern", password);
                startActivity(tiash);
            }

        });

    }

}

