package mapshere.corsair.com.shakeapp.Games;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.lfk.justweengine.Engine.Engine;
import com.lfk.justweengine.Engine.GameTextPrinter;
import com.lfk.justweengine.Engine.GameTexture;
import com.lfk.justweengine.Info.UIdefaultData;
import com.lfk.justweengine.Sprite.BaseSprite;
import com.lfk.justweengine.Sprite.BaseSub;

/**
 * Created by AlZihad on 9/3/2016.
 */
public class QuickGame extends Engine {
    GameTextPrinter printer;
    Paint paint;
    Canvas canvas;
    Bitmap backGround2X;
    Rect bg_rect;
    GameTexture enemyPic;
    BaseSprite bullet;
    public QuickGame()
    {
        super(false);
        paint = new Paint();
        canvas = null;
        printer = new GameTextPrinter();
        printer.setTextColor(Color.BLACK);
        printer.setTextSize(54);
        //printer.
        printer.setLineSpaceing(28);
    }
    @Override
    public void init()
    {
        super.setScreenOrientation(ScreenMode.LANDSCAPE);
        UIdefaultData.init(this);
    }

    @Override
    public void load() {
        GameTexture tex = new GameTexture(this);
        if (!tex.loadFromAsset("pic/box.png")) {
            fatalError("Error loading space");
        }
        backGround2X = Bitmap.createBitmap(
                UIdefaultData.screenWidth,
                UIdefaultData.screenHeight * 2,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(backGround2X);
        Rect dst = new Rect(0, 0, UIdefaultData.screenWidth - 1,
                UIdefaultData.screenHeight);
        canvas.drawBitmap(tex.getBitmap(), null, dst, null);
        dst = new Rect(0, UIdefaultData.screenHeight,
                UIdefaultData.screenWidth,
                UIdefaultData.screenHeight * 2);
        canvas.drawBitmap(tex.getBitmap(), null, dst, null);
        bg_rect = new Rect(0, 0, UIdefaultData.screenWidth, UIdefaultData.screenHeight);

        enemyPic = new GameTexture(this);
        enemyPic.loadFromAsset("pic/box.png");
    }

    @Override
    public void draw() {
        canvas = super.getCanvas();
        canvas.drawBitmap(backGround2X, bg_rect, bg_rect, paint);
        printer.setCanvas(canvas);
        printer.drawText("Engine demo", 20, 50);

    }

    @Override
    public void update() {

        bullet = new BaseSprite(this);
        bullet.setTexture(enemyPic);
        bullet.setDipScale(150,80);
        addToSpriteGroup(bullet);
    }

    @Override
    public void touch(MotionEvent event) {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                int startX = (int) event.getX();
                int startY = (int) event.getY();
                Log.d("pos","x = " +startX+"  y= "+startY);
                break;

        }
    }

    @Override
    public void collision(BaseSub baseSub) {

    }
}
