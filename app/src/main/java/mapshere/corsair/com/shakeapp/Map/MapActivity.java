package mapshere.corsair.com.shakeapp.Map;

/**
 * Created by AlZihad on 8/29/2016.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mapshere.corsair.com.shakeapp.LockScreen.UpdateService;
import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.Constant;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    GPSTracker gps;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener listener;
    Location mlocation;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    IntentFilter mIntentFilter;
    Location loc;
    MarkerOptions mk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_content);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Constant.BROADCASTLOCATION);
        locationManager = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);

        ImageView share = (ImageView) findViewById(R.id.share);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                //t.append("\n " + location.getLongitude() + " " + location.getLatitude());
                mlocation = location;
                LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions().position(sydney).title("Markerpopopo"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        // locationManager.requestLocationUpdates("gps", 5000, 0, listener);

        startService(new Intent(getBaseContext(), GPSTracker.class));

        shareDialog = new ShareDialog(this);
        // this part is optional

        callbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                configure_button();
            }
        });

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        configure_button();
        registerReceiver(broadcastReceiver, mIntentFilter);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng)
            {
                configure_button();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                configure_button();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    void configure_button()
    {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }

        if (loc != null && mMap != null) {
            Log.d("Location: ", loc.toString());
            LatLng sydney = new LatLng(loc.getLatitude(), loc.getLongitude());

            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            CameraPosition position = new CameraPosition.Builder().target(sydney).zoom(17).bearing(19).tilt(30).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

            String cityName = "not specified";
            Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                if (addresses.size() > 0) {
                    for (int i = 0; i < addresses.size(); i++) {
                        Log.d("adList: ", addresses.get(i).getLocality());
                    }
                    System.out.println(addresses.get(0).getLocality());
                    cityName = addresses.get(0).getLocality();

                    if (ShareDialog.canShow(ShareLinkContent.class)) {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle("Asking for Help")
                                .setContentDescription(
                                        "Hello everyone, I am facing some security problem nearby. Help me ASAP")
                                .setContentUrl(Uri.parse("http://maps.google.com/maps/@" + loc.getLatitude() + "," + loc.getLongitude() + ",20z"))
                                .build();

                        shareDialog.show(linkContent);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    void showLock() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                            , 10);
                }
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null)
            {
                Toast.makeText(getApplicationContext(),"loc: "+location.getLatitude(),Toast.LENGTH_LONG).show();
              //  latitude = location.getLatitude();
               // longitude = location.getLongitude();

            } else {
                Toast.makeText(
                        getApplicationContext(),
                        "Location Null", Toast.LENGTH_SHORT).show();
            }
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (intent.getAction().equals(Constant.BROADCASTLOCATION))
            {
                ArrayList data = intent.getIntegerArrayListExtra("data");
                if (data !=null)
                {
                    Location location = (Location) data.get(0);
                    if (location!= null)
                    {
                        loc = location;
                        Toast.makeText(getApplicationContext(),"received successfull: "+ loc.getLatitude(),Toast.LENGTH_LONG).show();
                        LatLng sydney = new LatLng(loc.getLatitude(), loc.getLongitude());
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        CameraPosition position = new CameraPosition.Builder().target(sydney).zoom(20).bearing(19).tilt(30).build();
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                        mk = new MarkerOptions().position(sydney).title("your current position");
                        mMap.addMarker(mk);
                    }
                }
            }
        }
    };

    @Override
    public void onBackPressed()
    {
        stopService(new Intent(getApplicationContext(),GPSTracker.class));
    }
}