package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.os.Bundle;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.takwolf.android.lock9.Lock9View;

        import mapshere.corsair.com.shakeapp.R;

/**
 * Created by Tiash on 2/21/2016.
 */
public class LockChangeConfirm extends Activity
{
    Lock9View lock9View;
    String ss;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locksetup);
        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        final String hh = settings.getString("savedPattern", "ggg");
        TextView tt= (TextView) findViewById(R.id.text);
        tt.setText("Provide your Current Pattern");
        lock9View = (Lock9View) findViewById(R.id.lock_9_view);

        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
                if (password.matches(hh))
                {
                    // Toast.makeText()
                    startActivity(new Intent(getApplicationContext(),LockScreenSetup.class));
                }
            }

        });

    }
}

