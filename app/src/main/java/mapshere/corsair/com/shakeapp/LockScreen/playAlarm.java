package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.NotificationCompat;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import mapshere.corsair.com.shakeapp.MainActivity;
import mapshere.corsair.com.shakeapp.R;

public class playAlarm extends Activity
{
    MediaPlayer tias;
    NotificationManager mNotificationManager;
    private long mLastClickTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.siren);


        final AudioManager mAudioManager = (AudioManager) getSystemService (AUDIO_SERVICE);
        final int originalVolume =  mAudioManager.getStreamVolume (AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume (AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume (AudioManager.STREAM_MUSIC), 0);

        tias = (MediaPlayer) MediaPlayer.create(this,R.raw.mary);
        tias.start();
        tias.setLooping(true);
        Button stop= (Button) findViewById(R.id.stop);
        Intent intent = new Intent(this, playAlarm.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Build notification
        // Actions are just fake
        Context context = getApplicationContext();
        Intent notificationIntent = new Intent(context, playAlarm.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = context.getResources();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_launcher))
                .setTicker("Tiash")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("ALarm Confirmation")
                .setContentText("Tap here to turn off Alarm");
        Notification n = builder.getNotification();

        n.defaults |= Notification.DEFAULT_ALL;
        nm.notify(0, n);


        stop.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                /// tias.stop();
                //  startActivity(new Intent(getApplicationContext(), MainActivity.class));
                tias.stop();
                (playAlarm.this).finish();
                finish();
                Intent loginscreen = new Intent(getApplicationContext(), MainActivity.class);
                loginscreen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(loginscreen);
            }
        });



    }
	/*
	@Override
	public void onBackPfressed()
	{

  	  	tias.stop();
			finish();
			 startActivity(new Intent(getApplicationContext(), MainActivity.class));


	}*/


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_POWER))
        {
            tias.stop();
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                //Toast.makeText(getApplicationContext(), "Detected", Toast.LENGTH_LONG).show();
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }

        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP )
        {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 800)
            {
                //Toast.makeText(getApplicationContext(),"Detected too", Toast.LENGTH_LONG).show();
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            // Toast.makeText(getApplicationContext(),"Tap Again to Exit", Toast.LENGTH_LONG).show();
        }
        if (keyCode == KeyEvent.KEYCODE_BACK )
        { tias.stop();
            (playAlarm.this).finish();
            Intent loginscreen = new Intent(this, MainActivity.class);
            //loginscreen.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(loginscreen);
        }


        return true;
    }


}
