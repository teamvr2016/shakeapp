package mapshere.corsair.com.shakeapp;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.BaseViewAnimator;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import mapshere.corsair.com.shakeapp.Changemaker.ContactList;
import mapshere.corsair.com.shakeapp.Crack.Crack;
import mapshere.corsair.com.shakeapp.Crack.Utility;
import mapshere.corsair.com.shakeapp.Games.GameMenu;
import mapshere.corsair.com.shakeapp.Games.JavaGame;
import mapshere.corsair.com.shakeapp.Games.QuickGame;
import mapshere.corsair.com.shakeapp.Games.QuickQuiz;
import mapshere.corsair.com.shakeapp.LockScreen.LockChangeConfirm;
import mapshere.corsair.com.shakeapp.LockScreen.LockScreenSetup;
import mapshere.corsair.com.shakeapp.LockScreen.UpdateService;
import mapshere.corsair.com.shakeapp.Map.MapActivity;
import mapshere.corsair.com.shakeapp.SlidingTab.InfoZone;
import mapshere.corsair.com.shakeapp.SlidingTab.SlidingTabLayout;
import mapshere.corsair.com.shakeapp.SlidingTab.ViewPagerAdapter;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;
import mapshere.corsair.com.shakeapp.Utility.Constant;
import mapshere.corsair.com.shakeapp.WatchReport.AllReports;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {



    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    CardView play, info, changeMaker, marker, lock, watch, upload;
    ImageView playIcon, cover,lockImage;
    LinearLayout playList;
    int width;
    View view;
    Button playGame,playQuiz;
    int images[] = {R.mipmap.cover, R.mipmap.cover1, R.mipmap.cover2, R.mipmap.cover3, R.mipmap.cover4,R.mipmap.cover5, R.mipmap.cover6};


    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                presentShowcaseSequence();
            }
        });

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;

        // card view
        play = (CardView) findViewById(R.id.play);

        info = (CardView) findViewById(R.id.cardInfo);
        changeMaker = (CardView) findViewById(R.id.cardChangeMaker);
        marker = (CardView) findViewById(R.id.cardMarker);
        lock = (CardView) findViewById(R.id.cardLock);
        watch = (CardView) findViewById(R.id.cardWatch);
        upload = (CardView) findViewById(R.id.cardUpload);

        playGame = (Button) findViewById(R.id.play_game);
        playQuiz = (Button) findViewById(R.id.play_quiz);

        playIcon = (ImageView) findViewById(R.id.playicon);
        lockImage = (ImageView) findViewById(R.id.lockImage);
        cover = (ImageView) findViewById(R.id.cover);
        playList = (LinearLayout) findViewById(R.id.playlist);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        final LinearLayout mainLayout = (LinearLayout) findViewById(R.id.main_layout);

        playIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playList.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.SlideInLeft).duration(500).playOn(playList);
                YoYo.with(Techniques.Landing).duration(800).playOn(play);
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.RubberBand).duration(400).playOn(info);
                startActivity(new Intent(getApplicationContext(),InfoZone.class));
            }
        });
        changeMaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                YoYo.with(Techniques.RubberBand).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        startActivity(new Intent(getApplicationContext(), ContactList.class));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).duration(400).playOn(changeMaker);
            }
        });
        marker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.RubberBand).duration(400).playOn(marker);
                startActivity(new Intent(getApplicationContext(),MapActivity.class));
            }
        });
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                YoYo.with(Techniques.RubberBand)
                    .duration(200)
                    .interpolate(new AccelerateDecelerateInterpolator())
                    .withListener(new Animator.AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                            String state = settings.getString("available","no");
                            if (state.equals("no")) {
                                startActivity(new Intent(getApplicationContext(), LockScreenSetup.class));
                            }
                            else {
                                startActivity(new Intent(getApplicationContext(), LockChangeConfirm.class));
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {}

                        @Override
                        public void onAnimationRepeat(Animator animation) {}
                    }).playOn(lock);
            }
        });
        watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.RubberBand).duration(400).playOn(watch);
                startActivity(new Intent(getApplicationContext(), AllReports.class));
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.RubberBand).duration(400).playOn(upload);
                showUploadDialogue();
            }
        });

        playGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), GameMenu.class));
            }
        });

        playQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), QuickQuiz.class));
            }
        });

        View view = getCurrentFocus();

        Timer playTimer = new Timer();
        playTimer.schedule(new SwapPlayContent(playList), 5000);

        Timer coverTimer = new Timer();
        coverTimer.schedule(new CoverSwap(cover, 0), 0);

        appBarLayout.addOnOffsetChangedListener(
                new AppBarLayout.OnOffsetChangedListener() {
                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
                    {
                        if(collapsingToolbarLayout.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))
                        {
                            collapsingToolbarLayout.setTitle("ShakePatch");
                            mainLayout.setGravity(Gravity.CENTER);
                            ((FrameLayout.LayoutParams) mainLayout.getLayoutParams()).gravity = Gravity.CENTER_VERTICAL;
                        }
                        else
                        {
                            collapsingToolbarLayout.setTitle("");
                            mainLayout.setGravity(Gravity.NO_GRAVITY);
                            ((FrameLayout.LayoutParams) mainLayout.getLayoutParams()).gravity = Gravity.TOP;
                        }
                    }
                });

        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        boolean existance = settings.getBoolean("existance", false);
        String lockState = settings.getString("available","no");
        if (lockState.equals("yes"))
        {
            if (isMyServiceRunning(UpdateService.class)== false) {
                stopService(new Intent(getApplicationContext(),UpdateService.class));
                startService(new Intent(getApplicationContext(), UpdateService.class));
            }
            else
            {
                stopService(new Intent(getApplicationContext(),UpdateService.class));
                startService(new Intent(getApplicationContext(), UpdateService.class));
            }
        }

        if (existance == false   )
        {
            TakeInfo();
        }

    }

    private void showUploadDialogue()
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = layoutInflater.inflate(R.layout.upload_dlog, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);

        Button camOpen,galOpen,cancel;
        camOpen = (Button) v.findViewById(R.id.camera);
        galOpen =(Button) v.findViewById(R.id.gallery);
        cancel = (Button) v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
            }
        });

        camOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                boolean result= Utility.checkPermission(MainActivity.this);
                if (result)
                {
                    windowManager.removeViewImmediate(v);
                    cameraIntent();
                }
            }
        });

        galOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                boolean result= Utility.checkPermission(MainActivity.this);
                if (result)
                {
                    windowManager.removeViewImmediate(v);
                    galleryIntent();
                }
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(v, params);
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        submitCrackImage(thumbnail);
       // ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        submitCrackImage(bm);
      //  ivImage.setImageBitmap(bm);
    }

    public void submitCrackImage(Bitmap bm)
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = layoutInflater.inflate(R.layout.submit_image, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);
        ImageView crack;
        crack = (ImageView) v.findViewById(R.id.crack_image);
        try {
            bm = Bitmap.createScaledBitmap(bm,
                    (int) (bm.getWidth() * 0.3), (int) (bm.getHeight() * 0.3), false);
            crack.setImageBitmap(bm);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Error is image",Toast.LENGTH_LONG).show();
        }
        crack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
                showUploadDialogue();
            }
        });
        final EditText comment = (EditText) v.findViewById(R.id.comment);
        Button submit;
        submit = (Button) v.findViewById(R.id.submit);
        final Bitmap finalBm = bm;
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ///////////////////////////////////

                Toast.makeText(MainActivity.this, "Uploading Image. Please wait..." , Toast.LENGTH_LONG).show();
                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                String SERVER_URL = "http://ghorardim.ml/ShakeApp/dbquery.php";
                String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
                String imagePath = PATH + "/tiash";

                File file = new File(imagePath);

                OutputStream os = null;
                try {
                    os = new BufferedOutputStream(new FileOutputStream(file));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                finalBm.compress(Bitmap.CompressFormat.PNG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                /*
                params.put("query_name", "insertCrackReport");
                params.put("u_id", "2");
                params.put("time", "96789");
                params.put("comment", "what is what?");*/

                params.put("query_name", "insertCrackReport");
                params.put("u_id", asp.getUserId());
                params.put("time", System.currentTimeMillis()+"");
                params.put("comment", comment.getText().toString());
                try {
                    params.put("pic", new File(imagePath));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                client.post(SERVER_URL, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int i, Header[] headers, String s, Throwable throwable) {
                        Log.e("Faile", "Failure");
                        Toast.makeText(MainActivity.this, "Failed to upload" , Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onSuccess(int i, Header[] headers, String s) {
                        Log.e("Succ", "Success");
                        Toast.makeText(MainActivity.this, "Upload Success" , Toast.LENGTH_LONG).show();
                        windowManager.removeViewImmediate(v);
                    }
                });


                ///////////////////////////////////


              //  final ProgressDialog loading = ProgressDialog.show(getApplicationContext(),"Uploading...","Please wait...",false,false);
                /*StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/ShakeApp/dbquery.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s)
                            {
                                //loading.dismiss();
                                Toast.makeText(MainActivity.this, s , Toast.LENGTH_LONG).show();
                                windowManager.removeViewImmediate(v);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                //loading.dismiss();
                                Toast.makeText(MainActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                        }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        String image = getStringImage(finalBm);
                        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                        Map<String,String> params = new Hashtable<String, String>();
                        params.put("query_name", "insertCrackReport");
                        params.put("u_id", asp.getUserId());
                        params.put("time", System.currentTimeMillis()+"");
                        params.put("comment", comment.getText().toString());
                        params.put("pic", image);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);*/
            }
        });

        ImageView cancel;
        cancel = (ImageView) v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
            }
        });


        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(v, params);

    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(){

    }

    public void TakeInfo()
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.maindlog, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                width-200,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);

        android.support.v7.widget.AppCompatSpinner spinner = (android.support.v7.widget.AppCompatSpinner) view.findViewById(R.id.zilla);
        List<String> bloodlist = new ArrayList<String>();
        String zilla[] = {
                "Dhaka",
                "Barguna",
                "Barisal",
                "Bhola",
                "Jhalokati",
                "Patuakhali",
                "Pirojpur",
                "Bandarban",
                "Brahmanbaria",
                "Chandpur",
                "Chittagong",
                "Comilla",
                "Cox's Bazar",
                "Feni",
                "Khagrachhari",
                "Lakshmipur",
                "Noakhali",
                "Rangamati",
                "Faridpur",
                "Gazipur",
                "Gopalganj",
                "Kishoreganj",
                "Madaripur",
                "Manikganj",
                "Munshiganj",
                "Narayanganj",
                "Narsingdi",
                "Rajbari",
                "Shariatpur",
                "Tangail",
                "Bagerhat",
                "Chuadanga",
                "Jessore",
                "Jhenaidah",
                "Khulna",
                "Kushtia",
                "Magura",
                "Meherpur",
                "Narail",
                "Satkhira",
                "Jamalpur",
                "Mymensingh",
                "Netrakona",
                "Sherpur",
                "Bogra",
                "Joypurhat",
                "Naogaon",
                "Natore",
                "Chapainawabganj",
                "Pabna",
                "Rajshahi",
                "Sirajgonj",
                "Dinajpur",
                "Gaibandha",
                "Kurigram",
                "Lalmonirhat",
                "Nilphamari",
                "Panchagarh",
                "Rangpur",
                "Thakurgaon",
                "Habiganj",
                "Moulvibazar",
                "Sunamganj",
                "Sylhet"
        };

        for (int i=0;i<zilla.length;i++)
        {
            bloodlist.add(zilla[i]);
        }
        ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,bloodlist);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(dataAdapter);
        ImageView yes = (ImageView) view.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vie) {

                YoYo.with(Techniques.Wave)
                        .duration(200)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                                // chwck for internet, turn on Wifi/ data net

                            }

                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                                final EditText e1 = (EditText) view.findViewById(R.id.e1);
                                final EditText e2 = (EditText) view.findViewById(R.id.e2);
                                final EditText e3 = (EditText) view.findViewById(R.id.e3);
                                final AppCompatSpinner sp = (AppCompatSpinner) view.findViewById(R.id.zilla);

                                if (e1.getText().toString().length()> 1 && e2.getText().toString().length()> 6 &&e3.getText().toString().length()> 4 ) {
                                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                    StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/ShakeApp/dbquery.php",
                                            new Response.Listener<String>()
                                            {
                                                @Override
                                                public void onResponse(String response)
                                                {
                                                    //showToast(response.toString());
                                                    Log.d("Response", response);
                                                    Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_SHORT).show();

                                                    if (response.contains("TRUE"))
                                                    {
                                                        String ss[]=response.split(" ");
                                                        if (ss.length==2)
                                                        {
                                                            AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                                                            asp.putUserId(ss[1]);
                                                            YoYo.with(Techniques.FlipOutX)
                                                                    .duration(500)
                                                                    .interpolate(new AccelerateDecelerateInterpolator())
                                                                    .withListener(new Animator.AnimatorListener() {
                                                                        @Override
                                                                        public void onAnimationStart(Animator animation) {
                                                                        }

                                                                        @Override
                                                                        public void onAnimationEnd(Animator animation)
                                                                        {

                                                                            SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                                                                            //boolean existance = settings.getBoolean("existance", false);
                                                                            SharedPreferences.Editor edit = settings.edit();
                                                                            edit.putBoolean("existance", true);
                                                                            edit.putString("name", e1.getText().toString());
                                                                            edit.putString("NID", e2.getText().toString());
                                                                            edit.putString("Password",e3.getText().toString());
                                                                            edit.putString("zilla", sp.getSelectedItem().toString());
                                                                            Toast.makeText(getApplicationContext(), "You can Always Change info from preferences", Toast.LENGTH_SHORT).show();
                                                                            edit.commit();
                                                                            windowManager.removeViewImmediate(view);
                                                                            presentShowcaseSequence();
                                                                        }

                                                                        @Override
                                                                        public void onAnimationCancel(Animator animation) {
                                                                        }

                                                                        @Override
                                                                        public void onAnimationRepeat(Animator animation) {
                                                                        }
                                                                    }).playOn(view.findViewById(R.id.rel1));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(getApplicationContext(),"No Network! Please Check Internet Connection!!",Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener()
                                            {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    // error
                                                    //showToast(error.toString());
                                                    Log.d("Error.Response", error.toString());
                                                }
                                            }
                                    ) {
                                        @Override
                                        protected Map<String, String> getParams()
                                        {
                                            Map<String, String>  params = new HashMap<String, String>();
                                            params.put("query_name", "insertUser");
                                            params.put("NAME", e1.getText().toString());
                                            params.put("pass", e2.getText().toString());
                                            params.put("NID", e3.getText().toString());
                                            params.put("addr", sp.getSelectedItem().toString());
                                            return params;
                                        }
                                    };
                                    queue.add(postRequest);

                                    //windowManager.removeViewImmediate(view);
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(), "Please Provide valid name and mobile number", Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        }).playOn(view.findViewById(R.id.yes));
            }
        });

        ImageView noo = (ImageView) view.findViewById(R.id.noo);
        noo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vie) {


                YoYo.with(Techniques.Wave)
                        .duration(500)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                windowManager.removeViewImmediate(view);
                                Toast.makeText(getApplicationContext(), "Emergency information is important for your safety", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent (Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }
                        }).playOn(view.findViewById(R.id.rel1));
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 100;
        windowManager.addView(view, params);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SwapPlayContent extends TimerTask {
        LinearLayout playlist;

        SwapPlayContent(LinearLayout playlist) {
            this.playlist = playlist;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (playlist.getVisibility() == View.VISIBLE) {
                        YoYo.with(Techniques.Pulse).duration(800).playOn(play);
                        YoYo.with(Techniques.SlideOutRight).duration(500).withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                playlist.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).playOn(playList);
                        Timer timer = new Timer();
                        timer.schedule(new SwapPlayContent(playlist), 4000);
                    } else {
                        playlist.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.SlideInLeft).duration(500).playOn(playList);
                        YoYo.with(Techniques.Pulse).duration(800).playOn(play);

                        Timer timer = new Timer();
                        timer.schedule(new SwapPlayContent(playlist), 8000);
                    }
                }
            });
            this.cancel();
        }
    }

    public class CoverSwap extends TimerTask {
        ImageView imageView;
        int i;

        CoverSwap(ImageView imageView, int i) {
            this.imageView = imageView;
            this.i = i;
        }

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(),"val "+i,Toast.LENGTH_LONG).show();
                    i = i % 7;
                    YoYo.with(Techniques.Landing).duration(1000).playOn(imageView);
                    imageView.setImageResource(images[i]);
                    Timer timer2 = new Timer();
                    timer2.schedule(new CoverSwap(imageView, ++i), 5000);
                }
            });
            this.cancel();
        }
    }


    public void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(50);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, Constant.SHOWCASE_ID2);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
            }
        });
      //  play, info, changeMaker, marker, lock, watch, upload;
        sequence.setConfig(config);
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(info)
                        .setDismissText("GOT IT")
                        .setContentText("Learn about basic steps to perform during an Earthquake")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(changeMaker)
                        .setDismissText("GOT IT")
                        .setContentText("Add Emergency Contacts and Edit SMS settings")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(marker)
                        .setDismissText("GOT IT")
                        .setContentText("Perform a Safety Check and share in Facebook")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(lock)
                        .setDismissText("GOT IT")
                        .setContentText("Add a Lock Pattern for your Manual Lockscreen")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(watch)
                        .setDismissText("GOT IT")
                        .setContentText("Browse Crack reports about nearby Buildings, Roads and Infrastructures")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(upload)
                        .setDismissText("GOT IT")
                        .setContentText("Report a crack nearby you. Just take a Snap and Upload")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );


        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(play)
                        .setDismissText("GOT IT")
                        .setContentText("Play related Games and take part in Quizes to act smart during Earthquake")
                        .withRectangleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.start();
        MaterialShowcaseView.resetSingleUse(this,Constant.SHOWCASE_ID2);
        //asp.putShowcaseValueInEmergencyContact();
    }

}
