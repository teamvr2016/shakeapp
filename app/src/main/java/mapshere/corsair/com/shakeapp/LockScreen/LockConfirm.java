package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.takwolf.android.lock9.Lock9View;

import mapshere.corsair.com.shakeapp.MainActivity;
import mapshere.corsair.com.shakeapp.R;

/**
 * Created by Tiash on 2/16/2016.
 */
public class LockConfirm extends Activity {
    Lock9View lock9View;
    String ss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locksetup);

        Bundle extra = getIntent().getExtras();
        ss = extra.getString("pattern","  ");

        TextView tt= (TextView) findViewById(R.id.text);
        tt.setText("Confirm your Pattern");
        lock9View = (Lock9View) findViewById(R.id.lock_9_view);

        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
                if (password.matches(ss))
                {
                    Toast.makeText(getApplicationContext(), "Saved Settings", Toast.LENGTH_LONG).show();
                    SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = settings.edit();
                    edit.putString("savedPattern", password);
                    edit.putString("available","yes");
                    edit.commit();
                    startService(new Intent(getApplicationContext(), UpdateService.class));
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }

        });

    }
}
