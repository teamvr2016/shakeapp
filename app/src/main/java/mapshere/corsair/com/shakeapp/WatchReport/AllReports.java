package mapshere.corsair.com.shakeapp.WatchReport;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mapshere.corsair.com.shakeapp.Crack.Crack;
import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.SlidingTab.Video;

/**
 * Created by AlZihad on 9/10/2016.
 */
public class AllReports extends AppCompatActivity {
    ListView list;
    ReportAdapter listAdapetr;
    List topSongsList;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_reports);
        list = (ListView) findViewById(R.id.report_list);
        topSongsList = new ArrayList<Crack>();
        /*
        for (int i=0;i<25;i++)
        {
            Crack temp = new Crack(""+i,""+i,"Video"+i, "www.youtube.com","this is a long testing feature"+1);
            topSongsList.add(temp);
        }*/

        getAllReport();
        listAdapetr = new ReportAdapter(getApplicationContext(), topSongsList);
        list.setAdapter(listAdapetr);
    }

    public void getAllReport()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/ShakeApp/dbquery.php",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        showToast(response.toString());
                        Log.d("Response", response);
                        try {
                            JSONArray jArray = new JSONArray(response);
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);
                             //   showToast(job.toString());
                                String u_id = job.getString("USER_ID");
                                String report_id= job.getString("REPORT_ID");
                                String pic = job.getString("PICTURE");
                                String time = job.getString("DATE_TIME");
                                String comment = job.getString("COMMENT");
                                Crack temp = new Crack(u_id,report_id,pic,time,comment);
                                topSongsList.add(temp);
                                listAdapetr.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showToast(e.toString());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        showToast(error.toString());
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "getAllReports");

                return params;
            }
        };
        queue.add(postRequest);
    }

    public void showToast(String g)
    {
        Toast.makeText(getApplicationContext(),g,Toast.LENGTH_SHORT).show();
    }
    public class ReportAdapter extends BaseAdapter
    {
        Context c;
        List crack;
        ReportAdapter(Context c, List<Crack> crack)
        {
            this.c = c;
            this.crack = crack;
        }

        @Override
        public int getCount() {
            return crack.size();
        }

        @Override
        public Object getItem(int i) {
            return crack.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater=getLayoutInflater();
            view=inflater.inflate(R.layout.report_content,null);
            ImageView Cover = (ImageView)view.findViewById(R.id.image);
            TextView title=(TextView) view.findViewById(R.id.title);
            Crack temp = (Crack) topSongsList.get(i);
            Uri image = Uri.parse(temp.picture);
            showToast(image.toString());
            Picasso.with(getApplicationContext())
                    .load(image)
                    .placeholder(R.mipmap.ic_launcher)   // optional
                    .error(R.mipmap.cover)                            // optional// optional
                    .into(Cover);
            title.setText(temp.comment);
           /* TextView singer=(TextView) view.findViewById(R.id.description);
            Video topSongsForMainPage = topSong.get(i);
            title.setText(topSongsForMainPage.getTitle());
            singer.setText(topSongsForMainPage.getSubtitle());*/

            list.invalidate();
            return view;
        }
    }
}