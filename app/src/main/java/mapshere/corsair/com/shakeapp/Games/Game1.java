package mapshere.corsair.com.shakeapp.Games;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import mapshere.corsair.com.shakeapp.R;

/**
 * Created by AlZihad on 9/7/2016.
 */
public class Game1  extends Activity
{
    TextView tv[][];
    int seconds =120;
    String findVal;
    protected Timer timeTicker;
    private Handler timerHandler    = new Handler();
    protected   int     timeTickDown        = 10;
    TextView time,find;
    List reqVal;
    long startTime, endTime;
    int tv_id [][] =
            {
            {R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven, R.id.eight},
            {R.id.one1, R.id.two1, R.id.three1, R.id.four1, R.id.five1, R.id.six1, R.id.seven1, R.id.eight1},
            {R.id.one2, R.id.two2, R.id.three2, R.id.four2, R.id.five2, R.id.six2, R.id.seven2, R.id.eight2},
            {R.id.one3, R.id.two3, R.id.three3, R.id.four3, R.id.five3, R.id.six3, R.id.seven3, R.id.eight3},
            {R.id.one4, R.id.two4, R.id.three4, R.id.four4, R.id.five4, R.id.six4, R.id.seven4, R.id.eight4},
            {R.id.one5, R.id.two5, R.id.three5, R.id.four5, R.id.five5, R.id.six5, R.id.seven5, R.id.eight5},
            {R.id.one6, R.id.two6, R.id.three6, R.id.four6, R.id.five6, R.id.six6, R.id.seven6, R.id.eight6},
            {R.id.one7, R.id.two7, R.id.three7, R.id.four7, R.id.five7, R.id.six7, R.id.seven7, R.id.eight7},
    };

    int colors[]={
                    R.color.one, R.color.two, R.color.three, R.color.four, R.color.five, R.color.six, R.color.seven, R.color.eight,
                    R.color.one1, R.color.two1, R.color.three1, R.color.four1, R.color.five1, R.color.six1, R.color.seven1, R.color.eight1};

    @Override
    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        setContentView(R.layout.game_one);
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int fixedWidth = width/10;
        tv = new TextView[8][8];
        for (int i=0;i<8;i++)
        {
            for (int j=0;j<8;j++)
            {
                tv[i][j] = (TextView) findViewById(tv_id[i][j]);
                tv[i][j].setWidth(fixedWidth);
                tv[i][j].setHeight(fixedWidth);
            }
        }
        time = (TextView) findViewById(R.id.time);
        find = (TextView) findViewById(R.id.find);
        startDialog();
    }

    public void startDialog()
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = layoutInflater.inflate(R.layout.play_game, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);
        CardView play = (CardView) v.findViewById(R.id.play);
        ImageView info = (ImageView) v.findViewById(R.id.info);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
                startGame();
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                showToast("play this game");
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 0;
        windowManager.addView(v, params);
    }

    private void removefromList(String text,int r, int c)
    {
        int val = Integer.parseInt(text);
        for (int i=0;i<reqVal.size();i++)
        {
            Data x = (Data) reqVal.get(i);
            if (x.getData()==val)
            {
                reqVal.remove(i);
                //showToast("new size  "+reqVal.size());
                tv[r][c].setVisibility(View.GONE);
                if (checkAllClear())
                {
                    timeTicker.cancel();
                    tick.cancel();
                    endTime = System.currentTimeMillis();
                    //showToast("doneee; "+((endTime-startTime)/1000));
                    find.setText("Level Complete");
                    showLevelComplete();
                }
                else
                {
                    updateFinder();
                }
                return;
            }
        }
    }
    public  void startGame()
    {
        reqVal = new ArrayList<Data>();
        int count =0;
        for (int i=0;i<8;i++)
        {
            for (int j=0;j<8;j++)
            {
                Random val = new Random();
                int value = val.nextInt(100);
                if (value < 63 && value > -1)
                {
                    Log.d("col:  " ,""+value);
                    tv[i][j].setText(""+value);
                    tv[i][j].setGravity(Gravity.CENTER);
                    tv[i][j].setTextColor(getResources().getColor(R.color.eight1));
                    final int finalI = i;
                    final int finalJ = j;
                    reqVal.add(new Data(value));
                    tv[i][j].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            if (tv[finalI][finalJ].getText().toString().matches(findVal))
                                removefromList(tv[finalI][finalJ].getText().toString(),finalI,finalJ);
                            YoYo.with(Techniques.Wave).duration(250).playOn(tv[finalI][finalJ]);}
                    });
                }
                else
                {
                    value= value%15;
                    tv[i][j].setText("");
                    tv[i][j].setGravity(Gravity.CENTER);
                    tv[i][j].setTextColor(getResources().getColor(colors[value]));
                }
            }
        }


        startTime= System.currentTimeMillis();
       // startGame();
        updateFinder();
        timeTicker = new Timer("Ticker");
        timeTicker.scheduleAtFixedRate(tick, 0, 100); // 100 ms each
    }
    public void updateFinder()
    {
        Random val = new Random();
        int value = val.nextInt(reqVal.size());
        Data temp =(Data) reqVal.get(value);
        find.setText("Find: "+temp.getData());
        findVal=""+temp.getData();
    }

    public boolean checkAllClear()
    {
        if (reqVal.isEmpty())
            return true;
        else
            return false;
    }

    public class Data
    {
        private int value;
        Data(int x)
        {
            this.value = x;
        }
        int getData()
        {
            return value;
        }
    }

    public void showToast(String z)
    {
        Toast.makeText(getApplicationContext(),z,Toast.LENGTH_SHORT).show();
    }

    protected TimerTask tick = new TimerTask() {
        public void run() {
            myTickTask();
        }
    };

    // Override this in Subclass to get or add specific tick behaviors
    protected void myTickTask() {
        if (timeTickDown == 0) {
            timerHandler.post(doUpdateTimeout);
        }
        timeTickDown--;

    }

    private Runnable doUpdateTimeout = new Runnable() {
        public void run() {
            updateTimeout();
        }
    };

    private void updateTimeout() {
        timeTickDown = 10;
        seconds--;
        time.setText("Time: "+seconds+"s");
        Log.d("seee", ""+seconds);
        if (seconds ==0)
        {
           // timeTicker.
            timeTicker.cancel();
            tick.cancel();
            showGameOver();
        }
        else if (seconds>0 && seconds<11)
        {
            YoYo.with(Techniques.Shake)
                    .duration(250)
                    .playOn(time);
        }
    }

    public void showGameOver()
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = layoutInflater.inflate(R.layout.game_over, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);
        // CardView play = (CardView) v.findViewById(R.id.play);
        ImageView retry = (ImageView) v.findViewById(R.id.retry);
        ImageView back = (ImageView) v.findViewById(R.id.back);
        //

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
                finish();
            }
        });
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                windowManager.removeViewImmediate(v);
                startActivity(new Intent(getApplicationContext(),Game1.class));
            }
        });
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 0;
        windowManager.addView(v, params);
    }


    public void showLevelComplete()
    {
        final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = layoutInflater.inflate(R.layout.level_complete, null);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE,
                PixelFormat.TRANSLUCENT);
        // CardView play = (CardView) v.findViewById(R.id.play);
        TextView tv = (TextView) v.findViewById(R.id.value);
        tv.setText(""+seconds);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.x = 0;
        params.y = 0;
        windowManager.addView(v, params);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeViewImmediate(v);
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        timeTicker.cancel();
        tick.cancel();
        finish();
    }
}