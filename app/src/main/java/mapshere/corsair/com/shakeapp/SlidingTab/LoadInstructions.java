package mapshere.corsair.com.shakeapp.SlidingTab;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;

/**
 * Created by AlZihad on 9/7/2016.
 */
public class LoadInstructions extends Activity
{
    private WebView webview;
    private static final String TAG = "Main";
    private ProgressDialog progressBar;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.quiz_menu);
        WebView webview = new WebView(this);
        setContentView(webview);
        webview.getSettings().setJavaScriptEnabled(true);
        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        String url = asp.GetVideoToPlay();
        url = "http://docs.google.com/gview?embedded=true&url=" + url;
        webview.loadUrl(url);
    }
}