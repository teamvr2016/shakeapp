package mapshere.corsair.com.shakeapp.SlidingTab;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mapshere.corsair.com.shakeapp.R;

/**
 * Created by AlZihad on 8/24/2016.
 */
public class Video
{
    private String title,link,subtitle;
    Video(String title, String link,String subTitle)
    {
        this.link=link;
        this.subtitle = subTitle;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
