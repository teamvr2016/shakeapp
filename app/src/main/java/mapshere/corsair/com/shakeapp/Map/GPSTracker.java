package mapshere.corsair.com.shakeapp.Map;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import android.location.Criteria;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import mapshere.corsair.com.shakeapp.Utility.Constant;

/**
 * Created by rakib on 8/22/2016.
 */
@SuppressWarnings("MissingPermission")
public class GPSTracker extends Service {

    private final Context mContext = null;

    public LocationManager locationManager = null;
    public Criteria hdCrit = null;
    public String mlocProvider = null;
    public String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public long MIN_TIME = 2*60*1000;
    public float MIN_DISTANCE = (float) 5;
    public LocationListener locationListener;
    public Location loc;
    public String IMEI = null;
    public String DEVICEID =  null;
    public String ID = null;

    public String URL = "https://my-data-rak13.c9users.io/up_data.php";
    /**
     * Function to show settings alert dialog.
     * On pressing the Settings button it will launch Settings Options.
     * */
    public void showSettingsAlert()
    {
        Toast.makeText(GPSTracker.this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void sendLocationBroadcast()
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constant.BROADCASTLOCATION);
        ArrayList data = new ArrayList<Location>();
        data.add(loc);
        broadcastIntent.putParcelableArrayListExtra("data",data);
        sendBroadcast(broadcastIntent);
    }
    @SuppressWarnings("MissingPermission")
    public void  getLoc(){
//        locationManager.removeUpdates(locationListener);
//        locationManager.requestLocationUpdates(mlocProvider, MIN_TIME, MIN_DISTANCE, locationListener);
        loc = null;
        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(loc != null) {
           // Toast.makeText( GPSTracker.this, "From GPS: \n" +loc.getLongitude()+" "+loc.getLatitude() + " " + loc.getBearing(),  Toast.LENGTH_SHORT).show();
            sendLocationBroadcast();
        }
        else
        {
            loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (loc != null) {
               // Toast.makeText(GPSTracker.this, "From Network: \n" + loc.getLongitude() + " " + loc.getLatitude() + " " + loc.getBearing(), Toast.LENGTH_SHORT).show();
                sendLocationBroadcast();
            }
            else {
                Toast.makeText(GPSTracker.this, "Please turn on GPS and/or Internet Connection", Toast.LENGTH_SHORT).show();
                showSettingsAlert();
            }
        }
    }

    public void init()
    {
        Log.e("in init()", "Initializing");
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = telephonyManager.getDeviceId();
//        DEVICEID = Settings.Secure.ANDROID_ID;
        DEVICEID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        ID = DEVICEID+IMEI;

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSettingsAlert();
        }
        hdCrit = new Criteria();
        hdCrit.setAccuracy(Criteria.ACCURACY_COARSE);
        hdCrit.setAltitudeRequired(true);
        hdCrit.setBearingRequired(true);
        hdCrit.setCostAllowed(true);
        hdCrit.setPowerRequirement(Criteria.POWER_LOW);
        mlocProvider = locationManager.getBestProvider(hdCrit, true);
//        mlocProvider = LocationManager.GPS_PROVIDER;
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                sendLocationBroadcast();
               // Toast.makeText(GPSTracker.this, "From OnLocation change \n" + location, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
              //  Toast.makeText( GPSTracker.this, "Status changed",  Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderEnabled(String provider) {
              //  Toast.makeText( GPSTracker.this, "Provider Enabled",  Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
               // Toast.makeText( GPSTracker.this, "Provider Disabled " + Toast.LENGTH_SHORT,  Toast.LENGTH_SHORT).show();
                showSettingsAlert();
            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, (float) MIN_DISTANCE, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, (float)MIN_DISTANCE, locationListener);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getLoc();
                Log.e("in init()", "Initialized");
            }
        }, 3000);

    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public  int onStartCommand(Intent intent, int flag, int startId){
       // Toast.makeText(GPSTracker.this, "Service Started", Toast.LENGTH_SHORT).show();
        init();
        return  START_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
       // Toast.makeText(GPSTracker.this, "Service Stopped", Toast.LENGTH_SHORT).show();
    }



}
