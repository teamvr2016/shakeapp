package mapshere.corsair.com.shakeapp.SlidingTab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;

/**
 * Created by AlZihad on 7/10/2016.
 */
public class Instructions extends Fragment
{
        VideoAdapter listAdapetr;
        ListView list;
        List topSongsList;
        @Override
        public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

                View v = inflater.inflate(R.layout.info_video_list, container, false);
                topSongsList = new ArrayList<Video>();
              /*  Video temp1 = new Video("Earthquake Immediate Preparations", "http://ghorardim.ml/ShakeApp/contents/earhquakebanglapdf1.pdf","Blog post");
                Video temp2 = new Video("What to do in Earthquake", "http://ghorardim.ml/ShakeApp/contents/earhquakebanglapdf2.pdf","Bangla literature about earthquake");
                Video temp3 = new Video("Earthquake Preparations" , "http://ghorardim.ml/ShakeApp/contents/earhquakebanglapdf3.pdf","Bangla blog post");
                Video temp4 = new Video("Earthquake Preparation Steps" , "http://ghorardim.ml/ShakeApp/contents/earhquakebanglapdf4.pdf","Earthquake easy steps in Bangla");

                topSongsList.add(temp1);topSongsList.add(temp2);topSongsList.add(temp3);
                topSongsList.add(temp4);*/

                loadJSON();
                list = (ListView) v.findViewById(R.id.video_list);
                listAdapetr = new VideoAdapter(getActivity(), topSongsList);
                list.setAdapter(listAdapetr);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                //  Toast.makeText(getContext(),"Selected "+i,Toast.LENGTH_LONG).show();
                                AppSharedPreference asp = AppSharedPreference.getInstance(getContext());
                                Video v = (Video) topSongsList.get(i);
                                asp.VideoToPlay(v.getLink());
                                asp.VideoArtistName(v.getTitle());
                                startActivity(new Intent(getContext(),LoadInstructions.class));
                        }
                });
                return v;
        }
        public void loadJSON()
        {
            RequestQueue queue = Volley.newRequestQueue(getContext());
            StringRequest postRequest = new StringRequest(Request.Method.GET, "http://ghorardim.ml/ShakeApp/instructions/instructions.json",
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Response", response);
                            try {
                                // JSONArray jArray2 =
                                JSONArray jArray = new JSONArray(response);
                                for (int i=0;i<jArray.length();i++)
                                {
                                    JSONObject job = jArray.getJSONObject(i);
                                    String time = job.getString("sub");
                                    String place =job.getString("title") ;
                                    String link = job.getString("link");
                                    Video v = new Video(place,link,time);
                                    topSongsList.add(v);
                                    listAdapetr.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(),"Data loading error. Please check your connection...",Toast.LENGTH_LONG).show();
                            Log.d("Error.Response", error.toString());
                        }
                    }
            ) {

            };
            queue.add(postRequest);

        }
        public class VideoAdapter extends BaseAdapter
        {
                Activity activity;
                List<Video> topSong;

                public VideoAdapter(Activity activity, List<Video> topSong)
                {
                        super();
                        this.activity = activity;
                        this.topSong = topSong;
                }

                @Override
                public int getCount() {
                        return topSong.size();
                }

                @Override
                public Video getItem(int i) {
                        return topSong.get(i);
                }

                @Override
                public long getItemId(int i) {
                        return 0;
                }

                @Override
                public View getView(final int i, View view, ViewGroup viewGroup)
                {
                        LayoutInflater inflater=activity.getLayoutInflater();
                        view=inflater.inflate(R.layout.video_info_card,null);
                        ImageView topSongsCover = (ImageView)view.findViewById(R.id.image);
                        TextView title=(TextView) view.findViewById(R.id.title);
                        TextView singer=(TextView) view.findViewById(R.id.description);
                        Video topSongsForMainPage = topSong.get(i);
                        title.setText(topSongsForMainPage.getTitle());
                        singer.setText(topSongsForMainPage.getSubtitle());
                        String url = topSongsForMainPage.getLink();
                        topSongsCover.setImageResource(R.mipmap.watch);
                        return view;
                }
        }


}
