package mapshere.corsair.com.shakeapp.Utility;

/**
 * Created by AlZihad on 8/29/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class AppSharedPreference {
    private static AppSharedPreference mAppSharedPreference;

    private SharedPreferences mSharedPreferences;


    private Editor mEditor;
    private static Context mContext;

    /*
     * Implementing Singleton DP
     */
    private AppSharedPreference() {
        mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static AppSharedPreference getInstance(Context context) {
        mContext = context;
        if (mAppSharedPreference == null)
            mAppSharedPreference = new AppSharedPreference();
        return mAppSharedPreference;
    }

    // show the emergency number dialogue for the very first time use of the application
    public Boolean firstTimeAccess()
    {
        return mSharedPreferences.getBoolean(Constant.FirstTimeAccess,
                false);
    }
    public void putLogInUserToken(boolean token)
    {
        mEditor.putBoolean(Constant.FirstTimeAccess,token);
        mEditor.commit();
    }
    public String getEmergencyNumber1()
    {
        return mSharedPreferences.getString(Constant.EmergencyNumber1,"null");
    }
    public String getEmergencyNumber2()
    {
        return mSharedPreferences.getString(Constant.EmergencyNumber2,"null");
    }
    public void putEmergencyNumber1(String s)
    {
        mEditor.putString(Constant.EmergencyNumber1,s);
        mEditor.commit();
        //return mSharedPreferences.getString(Constant.EmergencyNumber1,"null");
    }
    public void putEmergencyNumber2(String s)
    {
        mEditor.putString(Constant.EmergencyNumber2,s);
        mEditor.commit();
    }

    public String getEmergencySMS()
    {
        return mSharedPreferences.getString(Constant.EmergencySMS,"Help me! I'm in danger. \n#ShakePatch");
    }
    public void putEmergencySMS(String s)
    {
        mEditor.putString(Constant.EmergencySMS,s);
        mEditor.commit();
    }

    public boolean getAdditionalSett()
    {
        return mSharedPreferences.getBoolean(Constant.AddSet,false);
    }
    public void putAdditionalSett(boolean s)
    {
        mEditor.putBoolean(Constant.AddSet,s);
        mEditor.commit();
    }

    public void putShowcaseValueInEmergencyContact()
    {
        mEditor.putBoolean("EmergecyShowcase",true);
        mEditor.commit();
    }

    public boolean getShowcaseValueInEmergencyContact()
    {
        return mSharedPreferences.getBoolean("EmergecyShowcase",false);
    }

    public void putUserId(String id)
    {
        mEditor.putString("USERID",id);
        mEditor.commit();
    }
    public String getUserId()
    {
        return mSharedPreferences.getString("USERID","null");
    }



    public void VideoToPlay(String s)
    {
        mEditor.putString("current",s);
        mEditor.commit();
    }

    public String GetVideoToPlay()
    {
        return mSharedPreferences.getString("current","hrdqtUGT4Rs");
    }

    public void VideoArtistName(String s)
    {
        mEditor.putString("artist",s);
        mEditor.commit();
    }

    public String GetVideoArtistName()
    {
        return mSharedPreferences.getString("artist","Error");
    }

    public void putimageLink(String s)
    {
        mEditor.putString("imageLink",s);
        mEditor.commit();
    }

    public String getimageLink()
    {
        return mSharedPreferences.getString("imageLink","error");
    }


}
