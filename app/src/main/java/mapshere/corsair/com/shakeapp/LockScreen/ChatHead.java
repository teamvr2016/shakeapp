package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */

/**
 * Created by Tiash on 2/17/2016.
 */
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.takwolf.android.lock9.Lock9View;

import java.security.Provider;

import mapshere.corsair.com.shakeapp.R;

import static android.view.View.inflate;

/**
 * Created by Tiash on 2/5/2016.
 */
public class ChatHead extends Service {

    private WindowManager windowManager;
    View view;
    //  private ImageView chatHead;
    ////LockScreenIntentReceiver mReceiver;
    @Override public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");

        registerReceiver(receiver,filter);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        inIt();

        return START_STICKY;
    }


    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // AudioManager amanager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
            // amanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if (TelephonyManager.EXTRA_STATE_RINGING.equals(state))
            {
                // amanager.setRingerMode(2);  //Ringer ON
                windowManager.removeViewImmediate(view);
            }
            if (TelephonyManager.EXTRA_STATE_IDLE.equals(state))
            {
                Toast.makeText(context,"dekhtesi bepar ta to",Toast.LENGTH_SHORT).show();
                inIt();
            }
            if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state))
            {
                Toast.makeText(context,"dekhtesi bepar ta",Toast.LENGTH_SHORT).show();

            }
        }
    };

    public void inIt ()
    {


        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        SharedPreferences settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        final String yy = settings.getString("savedPattern","000");
        view = inflate(getApplicationContext(), R.layout.locker, null);
        Lock9View lock9View = (Lock9View) view.findViewById(R.id.lock_9_view);
        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
                // Toast.makeText(ChatHead.this, yy, Toast.LENGTH_SHORT).show();
                if (password.matches(yy))
                {
                    stopService(new Intent(getApplicationContext(),ChatHead.class));
                    //st
                    Intent intent = new Intent(ChatHead.this,LockBackground.class);
                  /*  intent.addCategory(Intent.CATEGORY_HOME);*/
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("close", true);
                    getApplication().startActivity(intent);
                    windowManager.removeViewImmediate(view);
                }
            }

        });
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = 100;

        windowManager.addView(view, params);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //  unregisterReceiver(mReceiver);
    }


}

