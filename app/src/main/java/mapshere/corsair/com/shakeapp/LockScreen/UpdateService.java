package mapshere.corsair.com.shakeapp.LockScreen;

/**
 * Created by AlZihad on 9/4/2016.
 */



/**
 * Created by Tiash on 2/17/2016.
 */
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.security.Provider;

/**
 * Created by Tiash on 2/12/2016.
 */
public class UpdateService extends Service {

    BroadcastReceiver mReceiver;
    public static int countOn = 0;
    public static int countOff = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        // register receiver that handles screen on and screen off logic
        Log.i("UpdateService", "Started");
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_ANSWER);
        //  filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        mReceiver = new MyReceiver();
        registerReceiver(mReceiver, filter);

    }

    @Override
    public void onDestroy() {

        unregisterReceiver(mReceiver);
        Log.i("onDestroy Reciever", "Called");

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        boolean screenOn = intent.getBooleanExtra("screen_state", false);
        if (!screenOn) {
            Log.i("screenON", "Called");
            Log.i("viaService", "CountOn =" + countOn);

            //Toast.makeText(getApplicationContext(), "Screen on", Toast.LENGTH_LONG).show();
        } else {
            Log.i("screenOFF", "Called");
            Log.i("viaService", "CountOff =" + countOff);
            // Toast.makeText(getApplicationContext(), "Screen is Awake", Toast.LENGTH_LONG).show();
        }


        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}

