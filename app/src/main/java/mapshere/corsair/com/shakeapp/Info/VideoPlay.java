package mapshere.corsair.com.shakeapp.Info;

/**
 * Created by AlZihad on 9/5/2016.
 */

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayer.PlaylistEventListener;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;
import mapshere.corsair.com.shakeapp.Utility.Constant;


/*
 * A simple YouTube Android API demo application demonstrating the use of {@link YouTubePlayer}
 * programmatic controls.
 */
public class VideoPlay extends YouTubeFailureRecoveryActivity implements NavigationView.OnNavigationItemSelectedListener
{

    private static final ListEntry[] ENTRIES = {
            new ListEntry("Androidify App", "irH3OSOskcE", false),
            new ListEntry("Chrome Speed Tests", "nCgQDjiotG0", false),
            new ListEntry("Playlist: Google I/O 2012", "PL56D792A831D0C362", true)};

    private static final String KEY_CURRENTLY_SELECTED_ID = "currentlySelectedId";

    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer player;
    private TextView stateText;
    private ArrayAdapter<ListEntry> videoAdapter;
    private Spinner videoChooser;
    private Button playButton;
    private Button pauseButton;
    private EditText skipTo;
    private TextView eventLog;
    private StringBuilder logString;
    private RadioGroup styleRadioGroup;

    private MyPlaylistEventListener playlistEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;

    private int currentlySelectedPosition;
    private String currentlySelectedId;
    TextView artist,album,date;


    @SuppressWarnings("StatementWithEmptyBody")



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_play_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(myToolbar);


        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(Constant.DEVELOPER_KEY, this);
        playlistEventListener = new MyPlaylistEventListener();
        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();
        artist = (TextView) findViewById(R.id.artist);
        album = (TextView) findViewById(R.id.album);
        //date = (TextView) findViewById(R.id.date);


    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        this.player = player;
        player.setPlaylistEventListener(playlistEventListener);
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        player.setPlayerStyle(PlayerStyle.DEFAULT);
        if (!wasRestored) {
            playVideoAtSelection();
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubePlayerView;
    }

    private void playVideoAtSelection()
    {/*
        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        String url = asp.GetVideoToPlay();
        //.substring(31);
        if (url.length()>20)
            url = url.substring(32);
        artist.setText(asp.GetVideoArtistName());
        album.setText(asp.GetVideoAlbumName());
        Toast.makeText(getApplicationContext(),url,Toast.LENGTH_SHORT).show();
        player.cueVideo(url);*/
       /* ListEntry selectedEntry = ENTRIES[1];
        if (selectedEntry.id != currentlySelectedId && player != null)
        {
            currentlySelectedId = selectedEntry.id;
            if (selectedEntry.isPlaylist)
            {
                player.cuePlaylist(selectedEntry.id);
            } else
            {
                player.cueVideo(selectedEntry.id);
            }
        }*/
    }





    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putString(KEY_CURRENTLY_SELECTED_ID, currentlySelectedId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        currentlySelectedId = state.getString(KEY_CURRENTLY_SELECTED_ID);
    }





    private static final int parseInt(String intString, int defaultValue) {
        try {
            return intString != null ? Integer.valueOf(intString) : defaultValue;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":")
                + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }

    private String getTimesText() {
        int currentTimeMillis = player.getCurrentTimeMillis();
        int durationMillis = player.getDurationMillis();
        return String.format("(%s/%s)", formatTime(currentTimeMillis), formatTime(durationMillis));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }


    private final class MyPlaylistEventListener implements PlaylistEventListener {
        @Override
        public void onNext() {
            //    log("NEXT VIDEO");
        }

        @Override
        public void onPrevious() {
            //   log("PREVIOUS VIDEO");
        }

        @Override
        public void onPlaylistEnded() {
            //   log("PLAYLIST ENDED");
        }
    }

    private final class MyPlaybackEventListener implements PlaybackEventListener {
        String playbackState = "NOT_PLAYING";
        String bufferingState = "";
        @Override
        public void onPlaying() {
            playbackState = "PLAYING";
            // updateText();
            //      log("\tPLAYING " + getTimesText());
        }

        @Override
        public void onBuffering(boolean isBuffering) {
            bufferingState = isBuffering ? "(BUFFERING)" : "";
            //updateText();
            //     log("\t\t" + (isBuffering ? "BUFFERING " : "NOT BUFFERING ") + getTimesText());
        }

        @Override
        public void onStopped() {
            playbackState = "STOPPED";
            //   updateText();
            //     log("\tSTOPPED");
        }

        @Override
        public void onPaused() {
            playbackState = "PAUSED";
            //  updateText();
            //   log("\tPAUSED " + getTimesText());
        }

        @Override
        public void onSeekTo(int endPositionMillis) {
            ///          log(String.format("\tSEEKTO: (%s/%s)",
            formatTime(endPositionMillis);
            formatTime(player.getDurationMillis());
        }
    }

    private final class MyPlayerStateChangeListener implements PlayerStateChangeListener {
        String playerState = "UNINITIALIZED";

        @Override
        public void onLoading() {
            playerState = "LOADING";
            // updateText();
            /////    log(playerState);
        }

        @Override
        public void onLoaded(String videoId) {
            playerState = String.format("LOADED %s", videoId);
            // updateText();
            //   log(playerState);
        }

        @Override
        public void onAdStarted() {
            playerState = "AD_STARTED";
            //  updateText();
            //   log(playerState);
        }

        @Override
        public void onVideoStarted() {
            playerState = "VIDEO_STARTED";
            //   updateText();
            //    log(playerState);
        }

        @Override
        public void onVideoEnded()
        {
            playerState = "VIDEO_ENDED";
            //   updateText();
            //   log(playerState);
        }

        @Override
        public void onError(ErrorReason reason) {
            playerState = "ERROR (" + reason + ")";
            if (reason == ErrorReason.UNEXPECTED_SERVICE_DISCONNECTION) {
                // When this error occurs the player is released and can no longer be used.
                player = null;
                // setControlsEnabled(false);
            }
            //updateText();
            //    log(playerState);
        }

    }

    private static final class ListEntry {

        public final String title;
        public final String id;
        public final boolean isPlaylist;

        public ListEntry(String title, String videoId, boolean isPlaylist) {
            this.title = title;
            this.id = videoId;
            this.isPlaylist = isPlaylist;
        }

        @Override
        public String toString() {
            return title;
        }

    }

}
