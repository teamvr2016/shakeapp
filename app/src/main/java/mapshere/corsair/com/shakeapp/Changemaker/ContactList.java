package mapshere.corsair.com.shakeapp.Changemaker;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import mapshere.corsair.com.shakeapp.R;
import mapshere.corsair.com.shakeapp.Utility.AppSharedPreference;
import mapshere.corsair.com.shakeapp.Utility.Constant;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by AlZihad on 9/10/2016.
 */
public class ContactList extends AppCompatActivity {
    EditText num1,num2,sms;
    ImageView save1,save2,save3;
    CheckBox sendLoc;
    AppSharedPreference asp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list);
        asp = AppSharedPreference.getInstance(getApplicationContext());
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        sms = (EditText) findViewById(R.id.sms);

        save1 = (ImageView) findViewById(R.id.save1);
        save2 = (ImageView) findViewById(R.id.save2);
        save3 = (ImageView) findViewById(R.id.savesms);

        sendLoc = (CheckBox) findViewById(R.id.sendLoc);

        if (!asp.getEmergencyNumber1().matches("null"))
        {
            num1.setText(asp.getEmergencyNumber1());
        }
        if (!asp.getEmergencyNumber2().matches("null"))
        {
            num2.setText(asp.getEmergencyNumber2());
        }
        sms.setText(asp.getEmergencySMS());
        sendLoc.setChecked(asp.getAdditionalSett());

        save1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (num1.getText().toString().length()>8)
                {
                    asp.putEmergencyNumber1(num1.getText().toString());
                    showToast("updated number");
                }
                else
                {
                    showToast("Enter valid number");
                }
            }
        });

        save2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (num2.getText().toString().length()>8)
                {
                    asp.putEmergencyNumber2(num2.getText().toString());
                    showToast("updated number");
                }
                else
                {
                    asp.putEmergencyNumber2("null");
                    showToast("Number Deleted");
                }
            }
        });

        save3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sms.getText().toString().length()>20)
                {
                    asp.putEmergencySMS(sms.getText().toString());
                    showToast("updated Emergency SMS");
                }
                else
                {
                    showToast("Enter a sms with more than 20 characters");
                }
            }
        });

        sendLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asp.putAdditionalSett(sendLoc.isChecked());
                showToast("Updated settings");
            }
        });
        if (!asp.getShowcaseValueInEmergencyContact())
            presentShowcaseSequence();
    }

    public void showToast(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
    }

    public void presentShowcaseSequence() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(50);

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, Constant.SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
            }
        });

        sequence.setConfig(config);
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(save1)
                        .setDismissText("GOT IT")
                        .setContentText("Add your Quick Contacts & Stay Connected")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(save2)
                        .setDismissText("GOT IT")
                        .setContentText("Add more Contacts to ensure better safety")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(save3)
                        .setDismissText("GOT IT")
                        .setContentText("Edit your Emergency SMS")
                        .withCircleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(sendLoc)
                        .setDismissText("GOT IT")
                        .setContentText("Send your current location with Emergency SMS")
                        .withRectangleShape()
                        .setFadeDuration(200)
                        .build()
        );
        sequence.start();
        MaterialShowcaseView.resetSingleUse(this,Constant.SHOWCASE_ID);
        asp.putShowcaseValueInEmergencyContact();
    }

}
