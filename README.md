# **SHAKEPATCH** #

An emergency solution for Sudden Earthquake

Features include:

1. Instant ask for help ( access done from the lockscreen)
2. Share Crack Report
3. Get earthquake alert
4. Documents and videos for safety preparations
5. Share location with SMS and facebook integration
6. Instant Earthquake Safety Check


